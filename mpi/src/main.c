#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <getopt.h>
#include <string.h>
#include <sys/time.h>

#include "../include/suffix.h"
#include "../include/comms.h"
#include "../include/merge.h"
#include "../include/stringsort.h"
#include "../include/suffixarray.h"

#ifdef CEPH
    #include "../include/cephread.h"
    #include "../include/cephwrite.h"
#else
    #include "../include/mpiread.h"
    #include "../include/mpiwrite.h"
#endif

int usage(int argc, char* argv[]){
    printf("Correct usage: %s -f inputfile\n", argv[0]);
    return 0;
}

int main(int argc, char** argv)
{
    struct timeval full_start, full_end;
    gettimeofday(&full_start, NULL);
    MPI_Init(&argc, &argv);

    /*
     * Check for correct number of command line args
     */

    if(argc != 3){
        printf("No file specified\n");
        return usage(argc, argv);
    }

    /*
     * Process command line arguments
     */

    int opt;
    char* filename;

    int digit_opind = 0;
    while(1){

        int this_option_optind = optind ? optind : 1;
        int option_index = 0;
        static struct option long_options[] = {{"file", 1, 0, 'f'}};

        opt = getopt_long(argc, argv, "f:", long_options, &option_index);

        if (opt == -1){
            break;
        }

        switch(opt){
            case 'f':
                filename = optarg;
                break;

            default:
                MPI_Abort(MPI_COMM_WORLD, 10);
        }

    }

    /*
     * Gather MPI information
     */

    int size, rank;
    int i, j;

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /*
     * Get num of partitions k
     */
    int k;
    k = quad(size);

    int color;
    if(rank < k){
        color = 0;
    }else{
        color = 1;
    }

    int *comms = get_comms(k);


    MPI_Comm read_comm;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &read_comm);

    if(rank < k || rank == size-1){
        color = 1;
    }else{
        color = 0;
    }

    MPI_Comm merge_comm;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &merge_comm);

    if(rank == size-1){
        color = 0;
    }else{
        color = 1;
    }

    MPI_Comm write_comm;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &write_comm);

    if(rank < k){
        char* suffix;
        //char** suffixarray;
        Suffix_t* suffixarray;
        struct timeval read_start, read_end;

        gettimeofday(&read_start, NULL);
        suffix = file_read(argc, argv, filename, k, rank, read_comm);
        gettimeofday(&read_end, NULL);
        int length = strlen(suffix);
        //printf("rank %d length = %d\n", rank, length);
        printf("rank %d read took %d\n\n", rank, 1000000*(read_end.tv_sec-read_start.tv_sec)+(read_end.tv_usec-read_start.tv_usec));
        //printf("%d: %d\n", rank, comms[rank]);

        int i;
        int upper = k*(k-1)/2 + k;
        if(rank == 0){
            for(i = k; i < upper; ++i){
                //printf("sending length %d to %d\n", length, i);
                MPI_Send(&length, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            }
        }


        suffixarray = createSuffixArray(suffix, comms, k);
        //for (i = 0; i < length; ++i) {
        //    printf("%d : %s\n", suffixarray[i].index, suffixarray[i].suffix);
        //}

        int num_comms = k*(k-1);
        MPI_Request req;
        int l;

        //int sends[num_comms / k];


        for(j = 0; j < num_comms; ++j){
            if(comms[j] == rank){
                for(i = 0; i < length; ++i){
                    /* Send to ranks k .. k*(k-1)/2 */
                    MPI_Isend(&(suffixarray[i].index), 1, MPI_INT, (j/2)+k, 0, MPI_COMM_WORLD, &req);
                    //printf("rank %d sending suffix[%d]\n", rank, i);
                }
            }
        }

        //printf("%d %d\n", rank, length);

        for(j = 0; j < num_comms; ++j){
            if(comms[j] == rank){
                for(i = 0; i < length; ++i){
                    /* Send to ranks k .. k*(k-1)/2 */
                    MPI_Isend(suffixarray[i].suffix, length-suffixarray[i].index, MPI_CHAR, (j/2)+k, 0, MPI_COMM_WORLD, &req);
                }
            }
        }
        /*
         * print each suffix array for debug
         */

        free(suffix);
        free(suffixarray);
    }else if(rank < size -1){
        int length;

        MPI_Status length_stat;
        MPI_Recv(&length, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &length_stat);

        Suffix_t* subSA1 = malloc(sizeof(Suffix_t) * length);
        Suffix_t* subSA2 = malloc(sizeof(Suffix_t) * length);

        MPI_Status subSA1_stat, subSA2_stat;

        for(i = 0; i < length; ++i){
            MPI_Recv(&(subSA1[i].index), 1, MPI_INT, comms[2*(rank-k)], 0, MPI_COMM_WORLD, &subSA1_stat);
        }
        for(i = 0; i < length; ++i){
            MPI_Recv(&(subSA2[i].index), 1, MPI_INT, comms[2*(rank-k)+1], 0, MPI_COMM_WORLD, &subSA2_stat);
        }

        /* allocate memory for subSA1 and subSA2 */
        for(i = 0; i < length; ++i){
            subSA1[i].suffix = malloc(sizeof(char) * (length - subSA1[i].index+1));
            subSA2[i].suffix = malloc(sizeof(char) * (length - subSA2[i].index+1));
            subSA1[i].suffix[length-subSA1[i].index] = '\0';
            subSA2[i].suffix[length-subSA2[i].index] = '\0';
        }

        /* Receive the suffixes into subSA1 and subSA2 */

        for(i = 0; i < length; ++i){
            MPI_Recv(subSA1[i].suffix, length-subSA1[i].index, MPI_CHAR, comms[2*(rank-k)], 0, MPI_COMM_WORLD, &subSA1_stat);
        }

        for(i = 0; i < length; ++i){
            MPI_Recv(subSA2[i].suffix, length-subSA2[i].index, MPI_CHAR, comms[2*(rank-k)+1], 0, MPI_COMM_WORLD, &subSA2_stat);
        }

        Suffix_t* orderedArray;
        orderedArray = merge(subSA1, subSA2, length);

        int num_suffices = 2 * length;

        if(rank == k){
            MPI_Send(&num_suffices, 1, MPI_INT, size-1, 0, MPI_COMM_WORLD);
        }

        MPI_Request merge_send_req;

        int r;
        for(r = k; r < size-1; ++r){
            MPI_Barrier(merge_comm);
            if(rank == r){
               //printf("rank %d is sending to rank %d\n", r, size-1);
               for(i = 0; i < num_suffices; ++i){
                   int suffix_length = strlen(orderedArray[i].suffix);
                   MPI_Send(&suffix_length, 1, MPI_INT, size-1, i, MPI_COMM_WORLD);
                   MPI_Send(orderedArray[i].suffix, suffix_length, MPI_CHAR, size-1, i, MPI_COMM_WORLD);
               }
            }
        }
        free(orderedArray);
    }else{

        file_write(argc, argv, filename, k, size, write_comm);
        gettimeofday(&full_end, NULL);
    }

    if(rank == size-1)
        printf("total time taken was %d\n", 1000000*(full_end.tv_sec-full_start.tv_sec)+(full_end.tv_usec-full_start.tv_usec));

    free(comms);

    MPI_Finalize();
    return 0;
}

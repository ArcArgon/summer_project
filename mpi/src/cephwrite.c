#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <rados/librados.h>
#include <sys/time.h>

#include "../include/cephwrite.h"


int file_write(int argc, char* argv[], char* filename, int k, int size, MPI_Comm write_comm)
{
    struct timeval write_start, write_end;
    int total_time = 0;

    char outfile_name[50];
    snprintf(outfile_name, sizeof(outfile_name), "%s.out", filename);

    int num_suffices;
    MPI_Status stat;

    /* Declare the cluster handle and required arguments. */
    rados_t cluster;
    char cluster_name[] = "ceph";
    char user_name[] = "client.admin";
    uint64_t flags;

    /* Initialize the cluster handle with the "ceph" cluster name and the "client.admin" user */
    int err;
    err = rados_create2(&cluster, cluster_name, user_name, flags);

    if (err < 0) {
            fprintf(stderr, "%s: Couldn't create the cluster handle! %s\n", argv[0], strerror(-err));
            exit(EXIT_FAILURE);
    }

    /* Read a Ceph configuration file to configure the cluster handle. */
    err = rados_conf_read_file(cluster, "/home/patrick/mycluster/ceph.conf");
    if (err < 0) {
            fprintf(stderr, "%s: cannot read config file: %s\n", argv[0], strerror(-err));
            exit(EXIT_FAILURE);
    }

    /* Read command line arguments */
    err = rados_conf_parse_argv(cluster, argc, argv);
    if (err < 0) {
            fprintf(stderr, "%s: cannot parse command line arguments: %s\n", argv[0], strerror(-err));
            exit(EXIT_FAILURE);
    }

    /* Connect to the cluster */
    err = rados_connect(cluster);
    if (err < 0) {
            fprintf(stderr, "%s: cannot connect to cluster: %s\n", argv[0], strerror(-err));
            exit(EXIT_FAILURE);
    }

    /* Open connection to pool */
    rados_ioctx_t io;
    char* poolname = "testpool";

    err = rados_ioctx_create(cluster, poolname, &io);
    if (err < 0) {
        fprintf(stderr, "%s: cannot open rados pool %s: %s\n", argv[0], poolname, strerror(-err));
        rados_shutdown(cluster);
        exit(1);
    }

    rados_completion_t comp;
    err = rados_aio_create_completion(NULL, NULL, NULL, &comp);
    if (err < 0) {
        fprintf(stderr, "%s: could not create aio completion: %s\n", argv[0], strerror(-err));
        rados_ioctx_destroy(io);
        rados_shutdown(cluster);

        exit(1);
    }

    MPI_Recv(&num_suffices, 1, MPI_INT, k, 0, MPI_COMM_WORLD, &stat);

    int r;

    char* suffix;
    int suffix_length;
    int offset = 0;

    int i;

    for(r = k; r < size-1; ++r){
        for(i = 0; i < num_suffices; ++i){
            MPI_Recv(&suffix_length, 1, MPI_INT, r, i, MPI_COMM_WORLD, &stat);
            suffix = malloc(sizeof(char) * (suffix_length+1));
            MPI_Recv(suffix, suffix_length, MPI_CHAR, r, i, MPI_COMM_WORLD, &stat);
            suffix[suffix_length] = '\0';

            gettimeofday(&write_start, NULL);
            err = rados_write(io, outfile_name, suffix, suffix_length+1, offset);
            gettimeofday(&write_end, NULL);
            total_time += 1000000*(write_end.tv_sec-write_start.tv_sec) + (write_end.tv_usec-write_start.tv_usec);
            //if(err < 0){
            //    fprintf(stderr, "%s: could not read object %s: %s\n", argv[0], filename, strerror(-err));
            //    rados_ioctx_destroy(io);
            //    rados_shutdown(cluster);
            //    exit(1);
            //}

            offset += (suffix_length+1);
            //printf("%s", suffix);

            free(suffix);
        }
    }
    printf("writes tool %d\n", total_time);
}



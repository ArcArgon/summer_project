/********************************************************
 * A Basic mpi file io implementation to test
 * parallel io capabilities
 * *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <mpi.h>

#include "../include/mpiread.h"

char* file_read(int argc, char* argv[], char* filename, int k, int rank, MPI_Comm read_comm)
{
    MPI_File input; //name of input file

    /*
     * Open file specified by filename in readonly mode.
     * Abort if a process fails to open the file
     */
    int mpi_err_code;
    mpi_err_code = MPI_File_open(read_comm, filename, MPI_MODE_RDONLY, MPI_INFO_NULL, &input);
    if(mpi_err_code != MPI_SUCCESS){
        printf("Rank %2d: Could not open file %s\n", rank, filename);
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    MPI_Offset offset; // Size of file

    MPI_File_get_size(input, &offset);
    offset--; // Ignore eof character

    /*
     *
     * Offset / sizeof(char) gives total num of chars in file.
     *
     */

    int nchars = offset / sizeof(char); // Total number of characters in string
    int npchars = nchars/k; // Number of characters each process gets
    int i;

    /*
     *
     * calculate npchars for each process using a for loop
     *
     */
    char* string = malloc(sizeof(char) * (npchars + 1)); // Extra char needed for terminator character;

    MPI_Status stat;
    offset = rank * (npchars) * sizeof(char);
    mpi_err_code = MPI_File_read_at(input, offset, string, npchars, MPI_CHAR, &stat);

    if(mpi_err_code != MPI_SUCCESS){
        printf("Rank %2d: Failed to read from file %s at offset %d\n", rank, filename, (int)offset);
        MPI_Abort(MPI_COMM_WORLD, -2);
    }

    string[npchars] = '\0';

    MPI_File_close(&input);

    return string;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#include "suffix.h"
#include "stringsort.h"
#include "merge.h"

Suffix_t* merge(Suffix_t* subSA1, Suffix_t* subSA2, int count)
{
    int rank;
    int size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int i;
    int k;

    Suffix_t* orderedArray = malloc(sizeof(Suffix_t) * count * 2);

    for(k = 0; k < count; ++k){
        int wordlen = strlen(subSA2[k].suffix);
        int j;
        orderedArray[k].suffix = malloc(sizeof(char) * wordlen + 1);
        orderedArray[k].suffix[wordlen] = '\0';
        for (j = 0; j < wordlen; ++j) {
            orderedArray[k].suffix[j] = subSA2[k].suffix[j];
        }

        orderedArray[k].index = subSA2[k].index;
    }

    for(k = 0; k < count; ++k){
        int full = 0;
        while(subSA2[full].index != 0){
            full++;
        }

        int length = strlen(subSA2[full].suffix);
        //printf("Full string at %d %s\n", full, subSA2[full].suffix);

        int newsize = length + strlen(subSA1[k].suffix);
        //printf("%d\n", newsize);

        //char* insertstring = malloc(sizeof(char) * (length + ;
        orderedArray[count+k].suffix = malloc(sizeof(char) * (newsize+1));
        orderedArray[count+k].suffix[newsize] = '\0';
        for(i = 0; i < strlen(subSA1[k].suffix); ++i){
            orderedArray[count+k].suffix[i] = subSA1[k].suffix[i];
        }
        orderedArray[count+k].index = count + k;

        strcat(orderedArray[count+k].suffix, subSA2[full].suffix);
    }


    free(subSA1); free(subSA2);

    ssort1(orderedArray, 2*count, 0);

    return orderedArray;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/suffix.h"
#include "../include/stringsort.h"

void printSuffixes(char** array, int n)
{
    int i;
    for (i = 0; i < n; ++i) {
        printf("%s\n", array[i]);
    }
}

void vecswap(int i, int j, int n, Suffix_t* suffixarray)
{
    while(n--){
        swap(i, j);
        i++;
        j++;
    }
}

int min(int a, int b){
    return a <= b ? a : b;
}

void ssort1(Suffix_t* suffixarray, int n, int depth)
{
    int a, b, c, d, r, v;

    if(n <= 1) return;

    a = rand() % n;
    swap(0, a);
    v = i2c(0);
    a = b = 1;
    c = d = n-1;

    for (;;){
        while(b <= c && (r = i2c(b)-v) <= 0){
            if(r == 0){
                swap(a, b);
                a++;
            }
            b++;
        }
        while(b <= c && (r = i2c(c)-v) >= 0){
            if(r == 0){
                swap(c, d);
                d--;
            }
            c--;
        }

        if(b > c) break;
        swap(b, c);
        b++;
        c--;
    }

    r = min(a, b-a);
    vecswap(0, b-r, r, suffixarray);

    r = min(d-c, n-d-1);
    vecswap(b, n-r, r, suffixarray);

    r = b - a;
    ssort1(suffixarray, r, depth);

    if (i2c(r) != 0) {
        ssort1(suffixarray+r, a+n-d-1, depth+1);
    }

    r = d - c;
    ssort1(suffixarray+n-r, r, depth);
}

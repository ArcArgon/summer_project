#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <sys/time.h>

#include "../include/mpiwrite.h"


int file_write(int argc, char* argv[], char* filename, int k, int size, MPI_Comm write_comm)
{
    struct timeval write_start, write_end;

    char outfile_name[50];
    snprintf(outfile_name, sizeof(outfile_name), "%s.out", filename);

    MPI_File outfile;

    MPI_File_open(write_comm, outfile_name, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &outfile);

    int num_suffices;
    MPI_Status stat;

    MPI_Recv(&num_suffices, 1, MPI_INT, k, 0, MPI_COMM_WORLD, &stat);

    int r;

    char* suffix;
    int suffix_length;
    int offset = 0;

    int i;

    int total_time = 0;

    for(r = k; r < size-1; ++r){
        for(i = 0; i < num_suffices; ++i){
            MPI_Recv(&suffix_length, 1, MPI_INT, r, i, MPI_COMM_WORLD, &stat);
            suffix = malloc(sizeof(char) * (suffix_length+1));
            MPI_Recv(suffix, suffix_length, MPI_CHAR, r, i, MPI_COMM_WORLD, &stat);
            suffix[suffix_length] = '\0';

            gettimeofday(&write_start, NULL);
            MPI_File_write_at(outfile, offset*sizeof(char), suffix, suffix_length+1, MPI_CHAR, &stat);
            gettimeofday(&write_end, NULL);
            total_time += 1000000*(write_end.tv_sec-write_start.tv_sec) + (write_end.tv_usec-write_start.tv_usec);

            offset += (suffix_length+1);

            free(suffix);
        }
    }

    printf("writes tool %d\n", total_time);

    MPI_File_close(&outfile);
}



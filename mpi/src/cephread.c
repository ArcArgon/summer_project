#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <rados/librados.h>
#include <inttypes.h>

#include "../include/cephread.h"

char* file_read(int argc, const char* argv[], char* filename, int k, int rank, MPI_Comm read_comm)
{

        /* Declare the cluster handle and required arguments. */
        rados_t cluster;
        char cluster_name[] = "ceph";
        char user_name[] = "client.admin";
        uint64_t flags;

        /* Initialize the cluster handle with the "ceph" cluster name and the "client.admin" user */
        int err;
        err = rados_create2(&cluster, cluster_name, user_name, flags);

        if (err < 0) {
                fprintf(stderr, "%s: Couldn't create the cluster handle! %s\n", argv[0], strerror(-err));
                exit(EXIT_FAILURE);
        }

        /* Read a Ceph configuration file to configure the cluster handle. */
        err = rados_conf_read_file(cluster, "/home/patrick/mycluster/ceph.conf");
        if (err < 0) {
                fprintf(stderr, "%s: cannot read config file: %s\n", argv[0], strerror(-err));
                exit(EXIT_FAILURE);
        }

        /* Read command line arguments */
        err = rados_conf_parse_argv(cluster, argc, argv);
        if (err < 0) {
                fprintf(stderr, "%s: cannot parse command line arguments: %s\n", argv[0], strerror(-err));
                exit(EXIT_FAILURE);
        }

        /* Connect to the cluster */
        err = rados_connect(cluster);
        if (err < 0) {
                fprintf(stderr, "%s: cannot connect to cluster: %s\n", argv[0], strerror(-err));
                exit(EXIT_FAILURE);
        }

        /* Open connection to pool */
        rados_ioctx_t io;
        char* poolname = "testpool";

        err = rados_ioctx_create(cluster, poolname, &io);
        if (err < 0) {
            fprintf(stderr, "%s: cannot open rados pool %s: %s\n", argv[0], poolname, strerror(-err));
            rados_shutdown(cluster);
            exit(1);
        }

        rados_completion_t comp;
        err = rados_aio_create_completion(NULL, NULL, NULL, &comp);
        if (err < 0) {
            fprintf(stderr, "%s: could not create aio completion: %s\n", argv[0], strerror(-err));
            rados_ioctx_destroy(io);
            rados_shutdown(cluster);

            exit(1);
        }

        uint64_t osize;
        time_t stamp;

        err = rados_aio_stat(io, filename, comp, &osize, &stamp);
        if(err < 0){
            fprintf(stderr, "%s: could stat object %s: %s\n", argv[0], filename, strerror(-err));
            rados_ioctx_destroy(io);
            rados_shutdown(cluster);
            exit(1);
        }

        rados_aio_wait_for_complete(comp); // in memory
        osize--;

        size_t nchars = (int)osize / sizeof(char);
        size_t npchars = nchars/k;

        uint64_t offset = rank * npchars;

        char* string = malloc(sizeof(char) * (npchars+1));
        if(string == NULL){
            printf("Malloc failed on rank %d\n", rank);
            rados_ioctx_destroy(io);
            rados_shutdown(cluster);
            exit(1);
        }

        /* Read from object at offset*rank */
        err = rados_read(io, filename, string, npchars, rank*offset);
        if(err < 0){
            fprintf(stderr, "%s: could not read object %s: %s\n", argv[0], filename, strerror(-err));
            rados_ioctx_destroy(io);
            rados_shutdown(cluster);
            exit(1);
        }

        string[npchars] = '\0';

        rados_aio_release(comp);

        rados_ioctx_destroy(io);
        rados_shutdown(cluster);

        return string;
}

/*****************************************************
 *
 *  Testing basic file io to read a full file in as a
 *  single string. Will need similar function in MPI
 *  to read in data from FASTA file.
 *
 *  May need to reformat input file using sed of awk
 *  to remove newline characters.
 *
 ****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int fileread(int argc, char *argv[])
{
    if(argc < 3){
        printf("Need more options\n");
        printf("Usage: %s -f 'filename'\n", argv[0]);
        return 2;
    }

    int opt;
    char* filename;
    while((opt =  getopt(argc, argv, "f:")) != -1){
        switch(opt){
            case 'f':
                filename = optarg;
                break;
        }
    }

    FILE* file_in =  fopen(filename, "r");

    if(file_in == NULL){
        printf("Couldn't open file: %s\n", filename);
        return 3;
    }

    fseek(file_in, 0, SEEK_END);
    long size = ftell(file_in);
    fseek(file_in, 0, SEEK_SET);

    printf("%d\n", size);

    char* fullstring = malloc(sizeof(char) * size);
    fread(fullstring, size, 1, file_in);
    fclose(file_in);

    printf("%s", fullstring);

    free(fullstring);

    return 0;
}

int main(int argc, char *argv[])
{
    fileread(argc, argv);
    return 0;
}

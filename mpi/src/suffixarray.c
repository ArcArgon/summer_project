#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
#include <string.h>

#include "../include/suffix.h"
#include "../include/stringsort.h"
#include "../include/suffixarray.h"

int charAt(char* arr, int d){
    if(d < strlen(arr)){
        return arr[d];
    }
    return -1;
}

/* an implementation of lsd suffix sort */
/* segfaults */
void lsd_string_sort(char** a, int lo, int hi, int d){
    int N = strlen(*a);
    int W = strlen(a[0]);
    int i, j;
    int* counts;

    char** tmp = malloc(sizeof(char*) * N);
    for (d = W-1; d >= 0; --d) {
        int* count = calloc(R, sizeof(int));
        counts = count;
        for (i = 0; i < N; ++i) {
            count[charAt(a[i], d)+1]++;
        }

        for (j = 1; j < R+1; ++j) {
            count[j] += count[j-1];
        }

        for (i = 0; i < N; ++i) {
            tmp[count[charAt(a[i], d)]++] = a[i];
        }

        for (i = 0; i < N; ++i) {
            a[i] = tmp[i];
        }

        //free(count);
    }

    free(tmp);
    //free(count);
}

Suffix_t* createSuffixArray(char* in_str, const int* comms, int k)
{
    int length = strlen(in_str);

    Suffix_t* suf_array = malloc(sizeof(Suffix_t) * length);

    int i, j;

    for(i = 0; i < length; ++i){
        suf_array[i].suffix = in_str++;
        suf_array[i].index = i;
    }

    ssort1(suf_array, length, 0);

    return suf_array;
}

void printSuffixArray(char** suffix_array, int lenght){
    int i;

    for (i = 0; i < lenght; ++i) {
        printf("%s\n", suffix_array[i]);
    }
    printf("\n\n");
}

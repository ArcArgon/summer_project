#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>

#include "../include/comms.h"

/* function to calculate the number of partitions from number of processes */
int quad(int size){
    float a, b, c;
    a = 0.5;
    b = 0.5;
    c = 1-size;
    float k = -b + sqrt(b*b - 4*a*c) / (2 * a);

    return k;
}

void sender(int* comms, int num_comms, int k){
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int *x = malloc(sizeof(int));
    *x = rank;

    MPI_Request req;
    int j;

    /* Send x to each corresponding recieve */
    for(j = 0; j < num_comms; ++j){
        if(comms[j] == rank){
            MPI_Isend(x, 1, MPI_INT, (j/2)+k, 0, MPI_COMM_WORLD, &req);
            //printf("Sending rank %d\n%d\n", rank, (j/2)+k);
            //MPI_Send(x, 1, MPI_INT, (j/2)+k, 0, MPI_COMM_WORLD);
        }
    }
}

void receiver(int* comms, int num_comms, int k){
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int* y = malloc(sizeof(int) * 2);
    int i;

    MPI_Status stat1;
    MPI_Status stat2;

    printf("%d %d %d \n", rank, comms[2*(rank-k)], comms[2*(rank-k)+1]);

    printf("%d %d %d\n", rank, y[0], y[1]);

    MPI_Recv(&y[0], 1, MPI_INT, comms[2*(rank-k)], 0, MPI_COMM_WORLD, &stat1);
    MPI_Recv(&y[1], 1, MPI_INT, comms[2*(rank-k)+1], 0, MPI_COMM_WORLD, &stat2);

    printf("%d %d %d\n", rank, y[0], y[1]);
}

/* Calculate the recvs for type I to type II comms */
int* get_comms(int k)
{
    int num_comms = k*(k-1);
    int* comms_array = malloc(sizeof(int) * num_comms);

    int i, j;
    int l = 0;
    for(i = 0; i < k; ++i){
        for(j = i+1; j < k; ++j){
            comms_array[l] = i;
            comms_array[l+1] = j;
            l += 2;
        }
    }

    return comms_array;
}

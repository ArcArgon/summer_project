#ifndef COMMS_H
#define COMMS_H

int quad(int size);
void sender(int* comms, int num_comms, int k);
void receiver(int* comms, int num_comms, int k);
int* get_comms(int k);

#endif /* end of include guard: COMMS_H */

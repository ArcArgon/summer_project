#ifndef MPIWRITE_H_QNM8CJ17
#define MPIWRITE_H_QNM8CJ17

int file_write(int argc, char* argv[], char* filename, int k, int size, MPI_Comm write_comm);

#endif /* end of include guard: MPIWRITE_H_QNM8CJ17 */

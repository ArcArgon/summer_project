#ifndef CEPHREAD_H
#define CEPHREAD_H

char* file_read(int argc, const char* argv[], char* filename, int k, int rank, MPI_Comm read_comm);

#endif /* end of include guard: CEPHREAD_H */

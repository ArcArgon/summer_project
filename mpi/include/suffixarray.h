#define R 256 // Radix

#ifndef SUFFIXARRAY_H
#define SUFFIXARRAY_H

int charAt(char* arr, int d);
void lsd_string_sort(char** a, int lo, int hi, int);
Suffix_t* createSuffixArray(char* in_str, const int* comms, int k);
void printSuffixArray(char** suffix_array, int lenght);

#endif /* end of include guard: SUFFIXARRAY_H */

#ifndef CEPHWRITE_H_EJB8V5AE
#define CEPHWRITE_H_EJB8V5AE

int file_write(int argc, char* argv[], char* filename, int k, int size, MPI_Comm write_comm);

#endif /* end of include guard: CEPHWRITE_H_EJB8V5AE */


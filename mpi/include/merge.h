#ifndef MERGE_H
#define MERGE_H

Suffix_t* merge(Suffix_t* subSA1, Suffix_t* subSA2, int count);

#endif /* end of include guard: MERGE_H */

#ifndef MPIREAD_H
#define MPIREAD_H

char* file_read(int argc, char* argv[], char* filename, int k, int rank, MPI_Comm read_comm);

#endif

set terminal png
set title "Size of output files for various input sizes and process counts"
set ylabel "Output size (Bytes)"
set xlabel "Processes"

plot './outsize.dat' us 1:2 w l title "4"
replot './outsize.dat' us 1:3 w l title "7"
replot './outsize.dat' us 1:4 w l title "11"
replot './outsize.dat' us 1:5 w l title "16"
set output "./outsize.png"
replot './outsize.dat' us 1:5 w l title "22"


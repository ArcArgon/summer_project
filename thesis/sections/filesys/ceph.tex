Ceph is a distributed file system that focuses on scalability and resilience.
It differs from file systems such as NFS in how data is handled.
NFS followed a simple paradigm where a central server exports a file system hierarchy to
clients who map it to their namespace.~\cite{NFSsimple}
They can then interact with it as if it were a local file system.

Recent file systems have moved towards object-based storage.
In object-based storage hard disks are replaced with intelligent Object Storage
Devices (OSD)~\cite{cephmain}.
OSDs combine a CPU, a network interface and a local cache with an underlying storage device,
usually a hard disk or RAID configuration.
By replacing the traditional block-level interface with OSDs, clients can perform large read and writes, while the devices themselves handle the distribution of low-level block allocations.
Clients interact with a Metadata Server (MDS) when performing metadata operations like opening or
renaming a file.
All I/O operations are handled directly by the OSDs.
Early object-based storage solutions suffered from a lack of MDS scalability~\cite{cephmain}.

Ceph overcomes this limitation by completely separating MDSs from ODSs.
Earlier object-based storage solutions replaced long per-file block lists with shorter object lists.
Ceph completely eradicates the use of allocation lists.
Data is striped across named objects and an algorithm called CRUSH handles the distribution and assignment of objects to the OSDs~\cite{cephmain}.
This results in a reduced workload for the metadata cluster since it no longer needs to store, maintain and distribute object lists.
When a client requests a file, the MDS can calculate the names and locations of all objects
that comprise the requested file.

Ceph storage is comprised of three major compononents.
The smallest storage units are OSDs.
A placement group is a group of OSDs.
The PGs determine which OSDs objects are spread across.
The largest unit are called pools.
Pools are the logical group for storing objects.
A pool consists of a number of placement groups and determines the number of replications made of each object.
By default each pool contains one hundred PGs and creates two replicas of data.

When creating a new pool, the number of PGs and replicas can be specified.
The commands in listing~\ref{lst:newpool} will create a new pool with 50 PGs and change the number of replicas from the default of two to three.

\begin{lstlisting}[caption=Creating a pool with 50 PGs and changing number of replicas to 3,label={lst:newpool}]
    # ceph osd pool create MyPool 50
    # ceph osd pool set MyPool size 3
\end{lstlisting}

A complete overview of the structure of a Ceph storage cluster is displayed in figure~\ref{fig:cephstore}.
As a general rule of thumb, the total number of placement groups is limited to one hundred per storage device.
Since two replicas are made of objects by default the usable number of PGs will be half the total.
This also applies to the capacity of the storage capacity of the cluster.


\begin{figure}[H]
    \centering
    \includegraphics
    [width=\textwidth]
    {images/ceph3.pdf}
    \caption{Overview of the Structure of a Pool}
    \label{fig:cephstore}
\end{figure}


\subsection{CRUSH}
CRUSH (Controlled Replication Under Scalable Hashing) is the algorithm that is responsible for the distribution of data across OSDs.
CRUSH uses a value relating to an object, usually an object ID, to deterministically map the value to a list of objects that store the data.
The distribution of the data is pseudo-random.

The CRUSH algorithm uses a per-device weight to distribute objects among storage devices. The resulting distribution of objects is approximately uniform~\cite{crushmain}.
For an input value $x$, CRUSH outputs a vector $\vec{R}$ containing n unique storage locations.
The per-device weight ensures that storage is balanced over the entire cluster, minimising the load placed on individual nodes.
Pseudocode for the algorithm used by CRUSH to determine the placement of an object in storage is shown in Algorithm~\ref{alg:crush}.

\begin{algorithm}[H]
    \caption{CRUSH placement for object x}
    \label{alg:crush}
    \begin{algorithmic}[1]
        \Procedure{TAKE}{$a$}
        \State{$\vec{i} \gets [a]$}\Comment{Place item $a$ in vector $\vec(i)$}
        \EndProcedure
        \Procedure{SELECT}{$n,t$}
        \State $\vec{o} \gets \theta$ \Comment{Initialise output to empty}
        \For{$i \in \vec{i}$}\Comment{loop over $\vec{i}$}
            \State $F \gets 0$ \Comment{Initialise failure counter to 0}
            \For{$r \gets 1,n$}\Comment{}
                \State $F_r \gets 0$
                \State $retry descent \gets false$\Comment{}
                \Repeat
                    \State $b \gets bucket(i)$
                    \State $retry\_bucket \gets false$
                    \Repeat
                        \If{"first n"}
                            \State $r' \gets r+f$
                        \Else
                            \State $r' \gets r+f_rn$
                        \EndIf
                            \algstore{crush}
                        \end{algorithmic}
                    \end{algorithm}

                    \begin{algorithm}[H]
                        \begin{algorithmic}[1]
                        \algrestore{crush}
                        \State $o \gets b.c(r',x)$
                        \If{$type(o) \neq t$}
                            \State $b \gets bucket(o)$
                            \State $retry\_bucket \gets true $
                        \ElsIf{$o \in \vec{o}$ or $failed(o)$ or $overload(o,x)$}
                            \State $f_r \gets f_r+1, f \gets f+1$
                            \If{$o \in \vec{o}$ and $f_r < 3$}
                                \State $retry\_bucket \gets true$ \Comment{}
                            \Else
                                \State $retry\_descent \gets true$
                            \EndIf
                        \EndIf
                    \Until
                \Until
                \State $\vec{o} \gets [\vec{o},o]$
            \EndFor
        \EndFor
        \State $\vec{i} \gets \vec{o}$
        \EndProcedure
        \Procedure{EMIT}{}
            \State $\vec{R} \gets [\vec{R},i]$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}


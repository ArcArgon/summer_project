set y2tics
set y2range [1.5:3.5]

plot 'times.dat' us 1:2 w l lw 1 axes x1y1 title '3 part'
replot 'times.dat' us 1:3 w l ls 2 axes x1y1 title 'qsort'
replot 'times.dat' us 1:4 smooth bezier axes x1y2 title 'Speed Up'

pause -1

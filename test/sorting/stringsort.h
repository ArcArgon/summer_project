
#ifndef STRINGSORT_H
#define STRINGSORT_H

#define swap(a, b) {char* tmp = suffixarray[a];\
    suffixarray[a] = suffixarray[b];\
    suffixarray[b] = tmp;\
}

#define i2c(i) suffixarray[i][depth]

void vecswap(int i, int j, int n, char** array);
void ssort1(char** suffixarray, int n, int depth);

#endif /* end of include guard: STRINGSORT_H */

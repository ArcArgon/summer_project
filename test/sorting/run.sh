#!/bin/bash

outfile="times.dat"

if [[ -f $outfile ]]; then rm $outfile; fi

size=100000

for i in $(seq 2); do
    ./main $size >> times.dat

    size=$(echo "$size+10000" | bc -l)
done

gnuplot plot.gp

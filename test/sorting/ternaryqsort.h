#ifndef TERNARYQSORT_H
#define TERNARYQSORT_H

void ternaryQSort(int* array, int left, int right);

#endif /* end of include guard: TERNARYQSORT_H */

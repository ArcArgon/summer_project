#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "ternaryqsort.h"

void ternaryQSort(int* array, int left, int right){
    if(left >= right) return ;

    /* Select pivot randomly from [left, right] */
    char pivot = array[left + (int)(drand48()*(right - left))];

    int i = left; /* start at left pivot and scan right */
    int mi = left; /* Startig position for next pivot */
    int j = right; /* start at right pivot and scan left */
    int mj = right; /* Position available for next pivot value */

    int tmp;

    for(;;){
        for(; i <= j && array[i] <= pivot; i++){
            if(array[i] == pivot){
                tmp = array[i];
                array[i] = array[mi];
                array[mi] = tmp;
                mi++;
            }
        }

        for(; i <= j && array[j] >= pivot; j--){
            if(array[j] == pivot){
                tmp = array[j];
                array[j] = array[mj];
                array[mj] = tmp;
                mj--;
            }
        }

        if(i > j) break;

        tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
        i++;
        j--;
    }

    for(mi--, i--; left <= mi; mi--, i--){
        tmp = array[i];
        array[i] = array[mi];
        array[mi] = tmp;
    }
    for(mj++, j++; mj <= right; mj++, j++){
        tmp = array[j];
        array[j] = array[mj];
        array[mj] = tmp;
    }
    ternaryQSort(array, left, i);
    ternaryQSort(array, j, right);
}

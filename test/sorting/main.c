#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "ternaryqsort.h"
#include <sys/time.h>

int cmp(const void* a, const void* b){
    const int* a1 = (const int*)a;
    const int* b1 = (const int*)b;
    return *a1 - *b1;
}

int main(int argc, char *argv[])
{
    if(argv[1] == NULL){
        printf("Need arg for N\n");
        return 1;
    }

    int N = atoi(argv[1]);
    int* array1 = malloc(sizeof(int) * N);
    int* array2 = malloc(sizeof(int) * N);
    int i;
    char* string = "SBDADLASNBDLJA";
    long int elapsed1, elapsed2;


    struct timeval t1, t2;

    for (i = 0; i < N; ++i) {
        array1[i] = rand() % 10;
        array2[i] = array1[i];
    }

    srand48(764247252752517);

    gettimeofday(&t1, NULL);
    ternaryQSort(array1, 0, N-1);
    gettimeofday(&t2, NULL);

    elapsed1 = (1000000 * (t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec);
    //printf("Ternary Split Quick Sort : %ld\n", elapsed1);

    gettimeofday(&t1, NULL);
    qsort(array2, N, sizeof(int), cmp);
    gettimeofday(&t2, NULL);

    elapsed2 = (1000000 * (t2.tv_sec - t1.tv_sec)) + (t2.tv_usec - t1.tv_usec);
    //printf("                   qsort : %ld\n", elapsed2);

    printf("%d, %ld, %ld, %lf\n", N, elapsed1, elapsed2, ((double)elapsed2 / (double)elapsed1));

    for(i = 0; i < N; i++){
        if(array1[i] != array2[i]){
            printf("element %d different: %d %d\n", i, array1[i], array2[i]);
        }
    }

    free(array1);
    free(array2);

    return 0;
}

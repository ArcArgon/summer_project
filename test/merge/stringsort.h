
#ifndef STRINGSORT_H
#define STRINGSORT_H

#define swap(a, b) {Suffix_t tmp = suffixarray[a];\
    suffixarray[a] = suffixarray[b];\
    suffixarray[b] = tmp;\
}

#define i2c(i) suffixarray[i].suffix[depth]

void vecswap(int i, int j, int n, Suffix_t* array);
void ssort1(Suffix_t* suffixarray, int n, int depth);

#endif /* end of include guard: STRINGSORT_H */

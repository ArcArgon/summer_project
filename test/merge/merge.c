#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "suffix.h"
#include "stringsort.h"
#include "merge.h"

Suffix_t* merge(Suffix_t* subSA1, Suffix_t* subSA2)
{
    int k = 0;
    //int count = 0;
    //while(subSA1[count] != NULL){
    //    count++;
    //}

    int count = 0;
    while(subSA2[count].suffix != NULL){
        count++;
    }

    Suffix_t* orderedArray = malloc(sizeof(Suffix_t) * count * 2);

    k = 0;
    while(subSA2[k].suffix != NULL){
        int wordlen = strlen(subSA2[k].suffix);
        int j;
        orderedArray[k].suffix = malloc(sizeof(char) * wordlen);
        for (j = 0; j < wordlen; ++j) {
            orderedArray[k].suffix[j] = subSA2[k].suffix[j];
        }

        orderedArray[k].index = subSA2[k].index;
        k++;
    }

    int i;

    for(k = 0; k < count; ++k){
        printf("%s\n", subSA1[k].suffix);
        int full = 0;
        while(subSA2[full].index != 0){
            full++;
        }

        int length = strlen(subSA2[full].suffix);
        printf("Full string at %d\n", full);

        int newsize = length + strlen(subSA1[k].suffix);
        printf("%d\n", newsize);

        //char* insertstring = malloc(sizeof(char) * (length + ;
        orderedArray[count+k].suffix = malloc(sizeof(char) * newsize);
        for(i = 0; i < strlen(subSA1[k].suffix); ++i){
            orderedArray[count+k].suffix[i] = subSA1[k].suffix[i];
        }
        orderedArray[count+k].index = count + k;

        strcat(orderedArray[count+k].suffix, subSA2[full].suffix);

    }

    free(subSA1); free(subSA2);

    ssort1(orderedArray, count+count, 0);

    return orderedArray;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "suffix.h"
#include "stringsort.h"
#include "merge.h"

void printSA(Suffix_t* array)
{
    int i = 0;
    printf(" i idx Suffix\n");
    while(array[i].suffix != NULL){
        printf("%2d %2d  %s\n", i, array[i].index, array[i].suffix);
        i++;
    }
}

int main(int argc, char *argv[])
{
    char* string1 = "BAN";
    char* string2 = "ANA";
    int N = strlen(string1);
    Suffix_t* suffixarray1 = malloc(sizeof(Suffix_t) * N);
    Suffix_t* suffixarray2 = malloc(sizeof(Suffix_t) * N);

    int i;

    for (i = 0; i < N; ++i) {
        suffixarray1[i].suffix = string1++;
        suffixarray1[i].index = i;
        suffixarray2[i].suffix = string2++;
        suffixarray2[i].index = i;
    }

    printf("---------------BEFORE---------------\n");
    printf("---------------Suffix 1---------------\n");
    printSA(suffixarray1);
    printf("---------------Suffix 2---------------\n");
    printSA(suffixarray2);

    ssort1(suffixarray1, N, 0);
    ssort1(suffixarray2, N, 0);

    printf("---------------AFTER---------------\n");
    printf("---------------Suffix 1---------------\n");
    printSA(suffixarray1);
    printf("---------------Suffix 2---------------\n");
    printSA(suffixarray2);

    printf("---------------MERGING---------------\n");
    Suffix_t* orderedArray = merge(suffixarray1, suffixarray2);

    printf("---------------MERGED---------------\n");
    printSA(orderedArray);

    /*
     * Free memory
     */

    //free(newString);
    //free(suffixarray1);
    //free(suffixarray2);

    i = 0;
    while(orderedArray[i].suffix != NULL){
        free(orderedArray[i].suffix);
        i++;
    }

    free(orderedArray);
    return 0;
}

#ifndef CEPHREAD_H
#define CEPHREAD_H

char* read_file(char* filename, int k, int rank, MPI_Comm read_comm);

#endif /* end of include guard: CEPHREAD_H */

#!/bin/bash

if [[ $1 == 'clean' ]]; then
    rm -rf input fa
    #echo "clean option set"
    exit 0
fi

# Set directories
gzdir=$(pwd)/gz
fadir=$(pwd)/fa
indir=$(pwd)/input

# Check for directories and add missing
echo "Checking directories"
if [[ ! -d $gzdir ]]; then
    echo "$gzdir not found, nothing to do"
    exit 1
fi

# Check if gzdir is empty
if [[ ! $(ls -A $gzdir) ]]; then
    echo "gzdir empty. Nothing to do"
    exit 2
else
    fasta_gz=$(ls $gzdir/*)
fi

if [[ ! -d $fadir ]]; then
    mkdir $fadir
fi

if [[ ! -d $indir ]]; then
    mkdir $indir
fi

# Decompress files in gzdir
echo "Decompressing files in gz"
for file in $fasta_gz; do
    out=$(echo $file | gawk 'match($0, /.*\/(.*fa)/, a) {print a[1]}')
    gzip --decompress -c $file > $fadir/$out
done

# tmp dir used for stroing files between tr and final awk
mkdir tmp

# Change lower case chars to upper case
fasta=$(ls $fadir/*)
echo "Chaging all characters to upper case"
for file in $fasta; do
    out=$(echo $file | gawk 'match($0, /.*\/(.*).fa/, a) {print a[1]}')
    cat $file | tr '[:lower:]' '[:upper:]' > tmp/$out.tmp
done

# Remove newline chars
tmpfiles=$(ls tmp/*)
echo "Removing newline characters"
for file in $tmpfiles; do
    out=$(echo $file | gawk 'match($0, /.*\/(.*).tmp/, a) {print a[1]}')
    cat $file | gawk NF=NF RS= OFS= > $indir/$out
done

# Delete tmp dir and files
echo "Cleaning up"
rm -rf tmp

echo "Done"
exit 0
